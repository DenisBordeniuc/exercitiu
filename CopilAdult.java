package exercitiu5;
import java.util.Scanner;

public class CopilAdult {
    public static void main(String[] args) {
        System.out.print("Introduceti virsta: ");
        Scanner in = new Scanner(System.in);
        int i = in.nextInt();
        in.close();
        if (i >= 0 && i <= 12) {
            System.out.println("Copil!");
        } else if (i >= 12 && i <= 18) {
            System.out.println("Adolescent!");
        } else if (i >= 18) {
            System.out.println("Adult!");
        } else {
            System.out.println("Ati introdus gresit! ");
        }
    }
}
